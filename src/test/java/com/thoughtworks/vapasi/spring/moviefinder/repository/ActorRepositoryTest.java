package com.thoughtworks.vapasi.spring.moviefinder.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@DataJpaTest
public class ActorRepositoryTest {


    @Autowired
    ActorRepository actorRepository;


    @Autowired
    TestEntityManager manager;

    @Test
    public void shouldAddActor()
    {
        Actor actor=new Actor("urvi");
        Actor saved=actorRepository.save(actor);
        Assertions.assertEquals(saved, manager.find(Actor.class,actor.getId()));
    }

    @Test
    public void shouldDeleteActor()
    {
        Actor actor=new Actor("urvi");
        Actor saved=manager.persist(actor);

        actorRepository.delete(saved);

        Assertions.assertNull(manager.find(Actor.class,saved.getId()));
    }

    @Test
    public void shouldReturnActors()
    {
        Actor actor1=new Actor("urvi");
        Actor actor2=new Actor("vipul");
        manager.persist(actor1);
        manager.persist(actor2);

        Assertions.assertEquals(actor1,actorRepository.findById(1));
        Assertions.assertEquals(2,actorRepository.findAll().size());
    }

    @Test
    public void shouldUpdateActor()
    {
        Actor actor1=new Actor("urvi");

        Actor saved=manager.persist(actor1);
        saved.setActorName("vipul");
        Actor actor=actorRepository.save(saved);
        Assertions.assertEquals("vipul",manager.find(Actor.class,saved.getId()).getActorName());
    }

}
