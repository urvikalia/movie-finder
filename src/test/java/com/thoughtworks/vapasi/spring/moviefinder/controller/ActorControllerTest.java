package com.thoughtworks.vapasi.spring.moviefinder.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.vapasi.spring.moviefinder.repository.Actor;
import com.thoughtworks.vapasi.spring.moviefinder.repository.ActorRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;

@AutoConfigureWebTestClient
@WebFluxTest(ActorController.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class ActorControllerTest {


    @Autowired
    ActorController actorController;

    @MockBean
    ActorRepository repository;

     @Autowired
    WebTestClient webTestClient;

    @Test
    public void testTest()
    {

        webTestClient.get().uri("/actors/test").exchange().expectStatus().isOk().
                expectBody().toString().equals("hello world");
    }


    @Test
    public void shouldAddActor()
    {

        Actor actor=new Actor("India");
        actor.setId(1);
        ObjectMapper objectMapper=new ObjectMapper();

        writeValueAsString(actor, objectMapper);

        Mockito.when(repository.save(actor)).thenReturn(actor);
        Actor saved=actorController.actorRepository.save(actor);
       webTestClient.post().uri("/actors").syncBody(actor).exchange()
               .expectStatus().isOk().expectBody().toString().
               equals("{\"id\":1,\"actorName\":\"India\"}");
      }


      private void writeValueAsString(Actor actor, ObjectMapper objectMapper) {
        try {
            String json =objectMapper.writeValueAsString(actor);
            System.out.println(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void shouldGetAllActors()
      {
          Actor actor=new Actor("India");
          actor.setId(1);
          Actor actor2=new Actor("Pakistan");
          actor2.setId(2);
          Mockito.when(repository.save(actor)).thenReturn(actor);
          Mockito.when(repository.save(actor2)).thenReturn(actor2);

          webTestClient.get().uri("/actors").exchange()
                  .expectStatus().isOk().expectBody().toString().
                  equals("[\n" +
                          "    {\n" +
                          "        \"id\": 1,\n" +
                          "        \"actorName\": \"India\"\n" +
                          "    },\n" +
                          "    {\n" +
                          "        \"id\": 2,\n" +
                          "        \"actorName\": \"Pakistan\"\n" +
                          "    }]");

      }

      @Test
    public void shouldDeleteActor()
      {
          Actor actor=new Actor("India");
          actor.setId(1);
          Mockito.when(repository.save(actor)).thenReturn(actor);
          webTestClient.delete().uri("/actors?actorid=1").
                exchange()
                  .expectStatus().isAccepted();
      }

      @Test
    public void shouldupdateActor()
      {
          Actor actor=new Actor("India");
          actor.setId(1);
          Mockito.when(repository.save(actor)).thenReturn(actor);

          webTestClient.put().uri("/actors").syncBody(actor).exchange().expectStatus().isOk();
      }

}
