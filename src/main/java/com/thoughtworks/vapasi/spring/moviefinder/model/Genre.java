package com.thoughtworks.vapasi.spring.moviefinder.model;

import com.thoughtworks.vapasi.spring.moviefinder.model.Movie;

import javax.persistence.*;

@Entity
public class Genre {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int genreId;


    private String genre;

    @OneToOne()
    @JoinColumn(name="movieId")
    private Movie movie;


    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }


    public Genre() {
        super();
    }

    public Genre(int genreId, String genre, Movie movie) {
        this.genreId = genreId;
        this.genre = genre;
        this.movie = movie;
    }
}
