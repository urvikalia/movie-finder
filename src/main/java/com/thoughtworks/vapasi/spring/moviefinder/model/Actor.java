package com.thoughtworks.vapasi.spring.moviefinder.repository;

import javax.persistence.*;


@Entity
public class Actor {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Column(name = "actorName")
    private String actorName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public Actor(int id, String actorName) {
        this.id = id;
        this.actorName = actorName;
    }

    public Actor() {

    }

    public Actor(String actorName) {
        this.actorName = actorName;
    }
}
