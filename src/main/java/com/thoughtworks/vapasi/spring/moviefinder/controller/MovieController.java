package com.thoughtworks.vapasi.spring.moviefinder.controller;

import com.thoughtworks.vapasi.spring.moviefinder.service.MovieService;
import com.thoughtworks.vapasi.spring.moviefinder.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/movies")
public class MovieController {


    @Autowired
    MovieService movieService;


    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody
    List<Movie> getMovies() {
            return movieService.getMovies();

    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public @ResponseBody
    Movie addMovie(@RequestBody Movie movie) {
       return movieService.addMovie(movie);

    }

}
