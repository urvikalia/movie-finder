package com.thoughtworks.vapasi.spring.moviefinder.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int movieId;

    @NotNull
    private String title;

    @NotNull
    private int yearOfRelease;

    @OneToOne(mappedBy = "movie",orphanRemoval = true,cascade = CascadeType.ALL)
    public Genre genre;


    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        movieId = movieId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }



    public Movie(int id,@NotNull String title, @NotNull int yearOfRelease, Genre genre) {
        this.movieId =id;
        this.title = title;
        this.yearOfRelease = yearOfRelease;
        this.genre = genre;
    }

    public Movie() {
        super();
    }
}
