package com.thoughtworks.vapasi.spring.moviefinder.model;

public class SearchCriteria {


    private String genre;
    private String movieName;

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }
}
