package com.thoughtworks.vapasi.spring.moviefinder.controller;

import com.thoughtworks.vapasi.spring.moviefinder.repository.Actor;
import com.thoughtworks.vapasi.spring.moviefinder.repository.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/actors")
public class ActorController {

    @Autowired
    ActorRepository actorRepository;


    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Actor> addActor(@RequestBody Actor actor)
    {
        //return actorRepository.save(actor);
        return ResponseEntity.ok(actorRepository.save(actor));

    }


    @RequestMapping(value = "",method = RequestMethod.GET)
    @ResponseBody
    public List<Actor> getActors()
    {
        return actorRepository.findAll();
    }

    @DeleteMapping(value = "")
    public @ResponseBody
    ResponseEntity<String> deleteActor(@RequestParam ("actorid") int actorId) {
        Actor deleteActor =actorRepository.findById(actorId);
        actorRepository.delete(deleteActor);
        return ResponseEntity.accepted().body("deleted");
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public @ResponseBody Actor updateActor(@RequestBody Actor actor) {
            Actor updateActor=actorRepository.save(actor);
            return updateActor;
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public @ResponseBody String greetings() {
        return "hello world";
    }


}
