package com.thoughtworks.vapasi.spring.moviefinder.service;

import com.thoughtworks.vapasi.spring.moviefinder.model.SearchCriteria;
import com.thoughtworks.vapasi.spring.moviefinder.model.Movie;
import com.thoughtworks.vapasi.spring.moviefinder.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;


    public List<Movie> getMovies() {

        List<Movie> movies=new ArrayList<>();
        for(Movie movie: movieRepository.findAll())
        {
            movies.add(movie);
        }
        return movies;
    }

    public Movie addMovie(Movie movie) {
        return movieRepository.save(movie);
    }

    public List<Movie> searchMovies(SearchCriteria searchCriteria) {
            return null;
    }

    public void deleteMovie(int id)
    {

    }

    public boolean updateMovie(Movie movie)
    {
        return true;
    }

}
