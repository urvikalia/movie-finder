package com.thoughtworks.vapasi.spring.moviefinder.repository;

import com.thoughtworks.vapasi.spring.moviefinder.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie,Long> {
}
