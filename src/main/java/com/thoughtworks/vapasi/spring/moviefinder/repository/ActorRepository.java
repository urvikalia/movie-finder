package com.thoughtworks.vapasi.spring.moviefinder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ActorRepository extends JpaRepository<Actor,Long> {
    Actor findById(int actorId);
}
